const http = require('http');
const url = require('url');

const server = http.createServer((req, res) => {
  const q = url.parse(req.url, true).query,
    a = +q.a || 0, b = +q.b || 0;

  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write(`${a+b}`);
  res.end();
});

server.listen(1337, "127.0.0.1");
